package payment.service;

import com.zheng.api.pojo.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import payment.mapper.PaymentMapper;

@Service
public class PaymentServiceImpl implements PaymentServiceI {
    @Autowired
    private PaymentMapper paymentMapperl;
    @Override
    public int toInsert(Payment payment) {
        return paymentMapperl.toInsert(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentMapperl.getPaymentById(id);
    }
}
