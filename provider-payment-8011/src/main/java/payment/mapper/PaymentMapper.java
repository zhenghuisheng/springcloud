package payment.mapper;

import com.zheng.api.pojo.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface PaymentMapper {
    //插入数据
    public int toInsert(Payment payment);
    //通过id查询数据
    public Payment getPaymentById(@Param("id") Long id);
}
