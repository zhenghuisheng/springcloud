package com.zheng.payment.controller;

import com.zheng.api.pojo.CommonResult;
import com.zheng.api.pojo.Payment;
import com.zheng.payment.service.PaymentServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
public class PaymentController {
    @Autowired
    private PaymentServiceImpl paymentService;
    //向外暴露服务
    @Autowired
    private DiscoveryClient discoveryClient;
    @PostMapping(value = "/payment/toInsert")
    public CommonResult toInsert(@RequestBody Payment payment){
        int result = paymentService.toInsert(payment);
        log.info("**********插入结果是" + result + "qaq");
        //无论数据是否插入成功，都得将数据给返回前端
        if(result > 0){
            return new CommonResult(200,"数据插入成功",result);
        }else{
            return new CommonResult(444,"插入数据失败",null);
        }
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id){
        Payment payment = paymentService.getPaymentById(id);
        log.info("**********查询的结果是" + payment + "qaq");
        //无论数据是否插入成功，都得将数据给返回前端
        //return new CommonResult(200,"数据查询成功！",payment);
        if(payment != null){
            return new CommonResult(200,"数据查询成功",payment);
        }else{
            return new CommonResult(444,"插入查询失败",null);
        }
    }

    @GetMapping("payment/discovery")
    public DiscoveryClient getDiscoveryClient(){
        List<String> services = discoveryClient.getServices();
        services.forEach(e-> System.out.println(e));
        List<ServiceInstance> instances = discoveryClient.getInstances("PAYMENT-SERVICE");
        instances.forEach(e-> System.out.println(e));
        return this.discoveryClient;
    }


    @GetMapping(value = "qaq")
    public String qaq(){
        return "hello world";
    }
}
