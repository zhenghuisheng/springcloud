package com.zheng.payment.service;

import com.zheng.api.pojo.Payment;
import org.apache.ibatis.annotations.Param;


public interface PaymentServiceI {
    //插入数据
    public int toInsert(Payment payment);
    //通过id查询数据
    public Payment getPaymentById(@Param("id") Long id);
}
