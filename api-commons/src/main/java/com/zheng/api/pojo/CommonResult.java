package com.zheng.api.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<T> {
    private Integer code;   //类似404
    private String message; //信息描述
    private T data;         //
    public CommonResult(Integer code,String message){
        this(code,message,null);
    }
}