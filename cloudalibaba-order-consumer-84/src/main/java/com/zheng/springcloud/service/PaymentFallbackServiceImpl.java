package com.zheng.springcloud.service;

import com.zheng.api.pojo.CommonResult;
import com.zheng.api.pojo.Payment;
import org.springframework.stereotype.Service;

/**
 * @author zhenghuisheng
 * @date 2020-2-19 14:22
 */
@Service
public class PaymentFallbackServiceImpl implements PaymentService {
    @Override
    public CommonResult<Payment> PaymentSQL(Long id) {
        return new CommonResult<>(444444444, "服务降级返回，----PaymentFallbackServiceImpl", new Payment(id, "errorSerial"));
    }
}
