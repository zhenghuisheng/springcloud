package com.zheng.springcloud.myhandler;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.zheng.api.pojo.CommonResult;

/**
 * @author zhenghuisheng
 * 创建customerBlockHandler类用于自定义限流处理逻辑
 * @date 2021-2-19
 */
public class CustomerBlockHandler {

    public static CommonResult handlerException(BlockException exception) {
        return new CommonResult(4444, "按客户自定义,global handlerException----------1");
    }

    public static CommonResult handlerException2(BlockException exception) {
        return new CommonResult(4444, "按客户自定义2,global handlerException----------2");
    }
}
