package com.zheng.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PaynentMain9001 {
    public static void main(String[] args) {
        SpringApplication.run(PaynentMain9001.class,args);
    }
}
