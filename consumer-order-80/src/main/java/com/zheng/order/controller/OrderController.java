package com.zheng.order.controller;

import com.zheng.api.pojo.CommonResult;
import com.zheng.api.pojo.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@Slf4j
public class OrderController {
    @Autowired
    private RestTemplate restTemplate;
    public static final String URL = "http://payment-service/";
    @GetMapping("/consumer/payment/toInsert")
    public CommonResult<Payment> toInsert(@RequestBody Payment payment){
        return restTemplate.postForObject(URL + "payment/toInsert",payment,CommonResult.class);
    }
    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPayment(@PathVariable Long id){
        return restTemplate.getForObject(URL+"payment/get/" + id,CommonResult.class);
    }

}
