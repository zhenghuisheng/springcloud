package com.zheng.order.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
//告诉springboot这是一个配置类，相当于以前在spring中的applicationContext.xml配置文件
public class ApplicationContextConfig {
    //既然是相当于以前的xml的配置文件，那么就可以在里面注入对象了
    @Bean
    @LoadBalanced       //开启负载均衡机制
    public RestTemplate getRest(){
        return new RestTemplate();
    }
}
